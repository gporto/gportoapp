//
//  AppDelegate.m
//  gportoApp
//
//  Created by Gerard Porto on 16/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "PhotoBrowserViewController.h"

#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        // reopen the session (without ui)
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends", @"read_stream", @"user_likes"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          [self launchUI:1];
                                      }];
    } else {
        // show login
        [self launchUI:0];
    }

    return YES;
}

-(void) launchUI:(NSInteger)isLogged {

    UINavigationController *navigationController;
    if (isLogged) {
        PhotoBrowserViewController *photoBrowser = [[PhotoBrowserViewController alloc] init];
        navigationController = [[UINavigationController alloc] initWithRootViewController:photoBrowser];
    } else {
        LoginViewController *login = [[LoginViewController alloc] init];
        navigationController = [[UINavigationController alloc] initWithRootViewController:login];
    }
    self.window.rootViewController = navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error
{
    NSLog(@"sessionStateChanged");
    switch (state) {
        case FBSessionStateOpen: {
            // call the method and pass the accessToken to it
            NSLog(@"FBSessionStateOpen");            
            break;
        }
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // Login failed or want to end the session
            [FBSession.activeSession closeAndClearTokenInformation];
            
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Facebook login failed"
                                  message:error.description
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    NSLog(@"handleOpenURL...");
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
