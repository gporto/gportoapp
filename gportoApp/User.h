//
//  GPUser.h
//  gportoApp
//
//  Created by Gerard Porto on 17/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *userUrlImage;
@property (nonatomic) NSInteger weight;

-(id)initWithUserId:(NSString *)userId andWeight:(NSInteger) weight;

@end
