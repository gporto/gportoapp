//
//  PhotoCell.m
//  gportoApp
//
//  Created by Gerard Porto on 17/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import "PhotoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PhotoCell

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imageView];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 86, 90, 20)];
        self.label.backgroundColor = [UIColor colorWithWhite:0.498 alpha:1.000];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = [UIColor whiteColor];
        self.label.text = @"weight";
        self.label.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.label];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = self.contentView.bounds;
}

-(void) setUser:(User *)user {

    [self.imageView sd_setImageWithURL:[NSURL URLWithString:user.userUrlImage] placeholderImage:[UIImage imageNamed:@"Image"] ];
    self.label.text = [NSString stringWithFormat:@" Weight: %ld",(long)user.weight];

}

@end
