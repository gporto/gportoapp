//
//  LoginViewController.h
//  gportoApp
//
//  Created by Gerard Porto on 16/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface LoginViewController : UIViewController <FBLoginViewDelegate>

@end
