//
//  GPUser.m
//  gportoApp
//
//  Created by Gerard Porto on 17/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import "User.h"

//@interface GPUser ()
//@property (nonatomic,strong) NSString *userId;
//@property (nonatomic,strong) NSString *userUrlImage;
//@property (nonatomic) NSInteger weight;
//@end

@implementation User

-(id)initWithUserId:(NSString *)userId andWeight:(NSInteger) weight{
    self = [super init];
    if(self)
    {
        self.userId = userId;
        self.weight = weight;
    }
    return self;
}


@end
