//
//  PhotoBrowserViewController.m
//  gportoApp
//
//  Created by Gerard Porto on 16/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import "PhotoBrowserViewController.h"
#import "PhotoCell.h"
#import <FacebookSDK/FacebookSDK.h>
#import "User.h"

@interface PhotoBrowserViewController ()
@property (nonatomic,strong) NSArray* topFBFriends;
@end

@implementation PhotoBrowserViewController

static NSString * const reuseIdentifier = @"photo";

- (instancetype) init {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(106, 106);
    layout.minimumInteritemSpacing = 1.0;
    layout.minimumLineSpacing = 1.0;
    
    self = [super initWithCollectionViewLayout:layout];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Photo Browser";
    self.collectionView.backgroundColor = [UIColor whiteColor];

    [self.collectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [self getUserTimeline];

}

- (void) refresh {
    [self.collectionView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.topFBFriends.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setUser:self.topFBFriends[indexPath.row]];
    
    return cell;
}

#pragma mark Find Top Friends

-(void) getUsersAvatar:(NSMutableDictionary *)dictTopFriends withFriendsId:(NSString *)strFriendsIds{
    
    [FBRequestConnection startWithGraphPath:@"picture"
                                 parameters:@{ @"ids" : strFriendsIds, @"type": @"normal",@"redirect" : @"false" }
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,id result,NSError *error) {
                              NSMutableArray *tmpFriends = [[NSMutableArray alloc] init];
                              // asign the url avatar to the user
                              for(id key in result){
                                  User *user = [dictTopFriends objectForKey:key] ;
                                  user.userUrlImage =[[[result objectForKey:key] objectForKey:@"data"] objectForKey:@"url"];
                                  [tmpFriends addObject:user];
                              }
                              
                              // sorting the array by 'weight'
                              self.topFBFriends = [tmpFriends sortedArrayUsingComparator: ^(User *u1, User *u2) {
                                  if ( u1.weight < u2.weight) { return (NSComparisonResult)NSOrderedDescending;}
                                  else if ( u1.weight > u2.weight) { return (NSComparisonResult)NSOrderedAscending;}
                                  return (NSComparisonResult)NSOrderedSame;
                              }];
                              
                              [self refresh];
                          }];
}

- (void) getUserTimeline {
    
    [FBRequestConnection startWithGraphPath:@"me/home"
                                 parameters:@{ @"fields" : @"from{id}", @"limit":@"100"}
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,id result,NSError *error) {
                              
                              // get all the users id from the timeline
                              NSArray *data = [result objectForKey:@"data"];
                              NSMutableArray *friendsInTimeline = [[NSMutableArray alloc] init];
                              for (NSDictionary *friend in data) {
                                  [friendsInTimeline addObject:[[friend objectForKey:@"from"] objectForKey:@"id"]];
                              }
                              
                              // number of user ocurrences (more ocurrences == better friend)
                              NSString *strFriendsIds = @"";
                              NSMutableDictionary *dictTopFriends = [[NSMutableDictionary alloc] init];
                              NSCountedSet *set = [[NSCountedSet alloc] initWithArray:friendsInTimeline];
                              for (id item in set) {
                                  User *user = [[User alloc] initWithUserId:item andWeight:(unsigned long)[set countForObject:item]];
                                  [dictTopFriends setObject:user forKey:item];
                                  strFriendsIds = [[strFriendsIds stringByAppendingString:item] stringByAppendingString:@","];
                              }
                              // remove the last ','
                              strFriendsIds = [strFriendsIds substringToIndex:strFriendsIds.length - (strFriendsIds.length>0)];
                              
                              [self getUsersAvatar:dictTopFriends withFriendsId:strFriendsIds];
                              
                          }];
    
}


@end
