//
//  LoginViewController.m
//  gportoApp
//
//  Created by Gerard Porto on 16/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import "LoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PhotoBrowserViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Login";
    
    FBLoginView *loginView = [[FBLoginView alloc] init];
    loginView.readPermissions = @[@"public_profile", @"user_friends", @"read_stream", @"user_likes"];
    loginView.frame = CGRectOffset(loginView.frame, (self.view.center.x - (loginView.frame.size.width / 2)), 200);
    loginView.delegate = self;
    [self.view addSubview:loginView];
    
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    PhotoBrowserViewController *photoBrowser = [[PhotoBrowserViewController alloc] init];
    self.navigationController.viewControllers =[NSArray arrayWithObject: photoBrowser];
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
