//
//  PhotoCell.h
//  gportoApp
//
//  Created by Gerard Porto on 17/1/15.
//  Copyright (c) 2015 Gerard Porto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface PhotoCell : UICollectionViewCell

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *label;

-(void) setUser:(User *)user;

@end

